#include "uls.h"

int main(int argc, char** argv) {
	ls_args_t ls_args = parse_args(argc, argv);
	ls_fs_elem_t* ls_fs = malloc(sizeof(ls_fs_elem_t));
	ls_fs_open(ls_fs, ls_args);
	print_ls_elem(ls_args.flags, ls_fs);
}
