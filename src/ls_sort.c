#include "uls.h"

int ls_sort_name (void* a, void* b) {
	return mx_strcmp(((ls_fs_elem_t*)a)->name, ((ls_fs_elem_t*)b)->name);
}
int ls_sort_time (void* a, void* b) {
	return ((ls_fs_elem_t*)b)->statbuf.st_mtime - ((ls_fs_elem_t*)a)->statbuf.st_mtime;
}
int ls_sort_size (void* a, void* b) {
	return ((ls_fs_elem_t*)b)->size - ((ls_fs_elem_t*)a)->size;
}

void ls_fs_sort_sub_elems(ls_fs_elem_t* fs, int(*cmp)(void*, void*)) {
	bubble_ptr_sort((void**)(fs->sub_elems), fs->elem_count, cmp);
}
