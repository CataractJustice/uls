#include "uls.h"

void mx_convert_size(int size){
    int pow = 0;
    float f_size = size;
    while(f_size >= 1000){
        f_size /= 1024;
        pow++;
    }

    if(f_size >= 10){
        if(f_size < 100) mx_printchar(' ');
        mx_printint((int)(f_size + .5f));
    }
    else{
        mx_printint((int)f_size);
        f_size += 0.05f;
        f_size *= 10;
        mx_printchar('.');
        mx_printint((int)f_size % 10);
    }

    switch(pow){
        case 0:
            mx_printstr("B");
            break;
        case 1:
            mx_printstr("K");
            break;
        case 2:
            mx_printstr("M");
            break;
        case 3:
            mx_printstr("G");
            break;
        case 4:
            mx_printstr("T");
            break;
    }

}

void mx_get_date(const struct stat fileStat, ls_flags_t flags){
    time_t t;
    time(&t);
    int new_time = 0;
    char *string;
    if(flags.u){
        string = ctime(&fileStat.st_atime);
        new_time = fileStat.st_atime;
    }
    else if(flags.c){
        string = ctime(&fileStat.st_ctime);
        new_time = fileStat.st_ctime;
    }
    else{
        string = ctime(&fileStat.st_mtime);
        new_time = fileStat.st_mtime;
    }
    
    string += 4;
    int dot_index = 0;
    for (dot_index = mx_strlen(string) - 1; string[dot_index] != ':'; dot_index--);
    if((t - new_time < 0 && -(t - new_time) < 3600) || (t - new_time >= 0 && (t - new_time) < 15552000)){
        string[dot_index] = '\0';
        mx_printstr(string);
    }
    else {
        char *new_string = NULL;
        int temp = 0;
        for(; string[temp] != ':'; temp++);
        temp -= 3;
        string[temp] = '\0';
        new_string = mx_strjoin(new_string, string);
        string[temp] = ' ';
        string += (dot_index + 3);
        new_string = mx_strjoin(new_string, " ");
        new_string = mx_strjoin(new_string, string);
        new_string[mx_strlen(new_string) - 1] = '\0';
        mx_printstr(new_string);
        free(new_string);
    }
}


void print_filetype_color(const ls_fs_elem_t* elem) {
    if(S_ISDIR(elem->statbuf.st_mode)) {
        if ((elem->statbuf.st_mode & S_ISVTX) && (elem->statbuf.st_mode & S_IWOTH)) {
            mx_printstr(BLACK_COLOR_FOR);
            mx_printstr(GREEN_COLOR_BACK);
        }
        else if (!(elem->statbuf.st_mode & S_ISVTX) && (elem->statbuf.st_mode & S_IWOTH)) {
            mx_printstr(BLACK_COLOR_FOR);
            mx_printstr(BROWN_COLOR_BACK);
        }
        else {
            mx_printstr(BLUE_COLOR_FOR);
        }
		return;
	}
    if(S_ISLNK(elem->statbuf.st_mode)) {
        mx_printstr(MAGENTA_COLOR_FOR);
		return;
	}
	if(S_ISSOCK(elem->statbuf.st_mode)) {
		mx_printstr(GREEN_COLOR_FOR);
	}
	if(S_ISFIFO(elem->statbuf.st_mode)) {
        mx_printstr(BROWN_COLOR_FOR);
		return;
	}
	if(S_ISBLK(elem->statbuf.st_mode)) {
        mx_printstr(BLUE_COLOR_FOR);
        mx_printstr(CYAN_COLOR_BACK);
		return;
	}
	if(S_ISCHR(elem->statbuf.st_mode)) {
        mx_printstr(BLUE_COLOR_FOR);
        mx_printstr(BROWN_COLOR_BACK);
		return;
	}

	if (S_IXUSR & elem->statbuf.st_mode) {
		if (elem->statbuf.st_mode & S_ISUID) {
			mx_printstr(BLACK_COLOR_FOR);
			mx_printstr(RED_COLOR_BACK);
		}
		else if (elem->statbuf.st_mode & S_ISGID) {
        	mx_printstr(BLACK_COLOR_FOR);
			mx_printstr(CYAN_COLOR_BACK);
		}
		else {
		mx_printstr(RED_COLOR_FOR);
		}

		return;
	}
	mx_printstr(NO_COLOR);
}

void print_scol(ls_flags_t flags, const ls_fs_elem_t* elem);
void print_cols (ls_flags_t flags, const ls_fs_elem_t* elem);

void print_ls_elem (ls_flags_t flags, const ls_fs_elem_t* elem) {
	if(flags.R) {
		mx_printstr(elem->full_name);
		mx_printstr(":\n");
	}
	
	if(!flags.l) {
		print_cols(flags, elem);
	}
	else {
		print_scol(flags, elem);
	}

	for(int j = 0; j < elem->elem_count; j++) {
		if(elem->sub_elems[j]->elem_count) {
			mx_printchar('\n');
			print_ls_elem(flags, elem->sub_elems[j]);
		}
	}
}


void print_cols (ls_flags_t flags, const ls_fs_elem_t* elem) {
	int console_cols = mx_get_console_cols();
	int max_len = 0;
	int tabwidth = 8;

	for(int j = 0; j < elem->elem_count; j++) {
		int name_len = mx_strlen(elem->sub_elems[j]->name);
		if(max_len < name_len)
			max_len = name_len;
	}

	int col_width = (max_len + tabwidth) & ~(tabwidth - 1);
	if (console_cols < 2 * col_width) {
		print_scol(flags, elem);
	}

	int ls_cols = console_cols / col_width;
	int ls_rows = elem->elem_count / ls_cols + 1;
	for (int row = 0; row < ls_rows; ++row) {
		int base = row;
		int endcol = col_width;
		for (int col = 0, chcnt = 0; col < ls_cols; ++col) {
			if(base >= elem->elem_count)
				break;
			chcnt += mx_strlen(elem->sub_elems[base]->name);
			print_filetype_color(elem->sub_elems[base]);
			mx_printstr(elem->sub_elems[base]->name);
			mx_printstr(NO_COLOR);
			base += ls_rows;
			int cnt;
			while ((cnt = ((chcnt + tabwidth) & ~(tabwidth - 1))) <= endcol) {
				mx_printchar('\t');
				chcnt = cnt;
			}
			endcol += col_width;
		}
		mx_printchar('\n');
	}
}

void print_scol(ls_flags_t flags, const ls_fs_elem_t* elem) {

	int column_size[8] = {8};

	char buffer[1024] = "";
    char value[1024];    
	acl_t acl;
    int xattr = 0;
	for(int i = 0; i < elem->elem_count; i++) {
		if(flags.l) {
			ls_print_mode(elem->sub_elems[i]);
			acl = acl_get_file(elem->sub_elems[i]->full_name, ACL_TYPE_ACCESS);
			int xattr_size = listxattr(elem->sub_elems[i]->full_name, buffer, 1024);
			xattr = getxattr(elem->sub_elems[i]->full_name, buffer, value, 1024);
			if(acl != NULL) mx_printchar('+');
			else if(xattr_size > 0) mx_printchar('@');
			else mx_printchar(' ');
			mx_printchar(' ');
			for(int j = 0; j < column_size[0] - mx_int_length(elem->sub_elems[i]->statbuf.st_nlink); j++) mx_printchar(' ');
			mx_printint(elem->sub_elems[i]->statbuf.st_nlink);
			mx_printchar(' ');

			struct passwd *pw = getpwuid(elem->sub_elems[i]->statbuf.st_uid);
			
			mx_printstr(pw->pw_name);
			for(int j = 0; j <= column_size[1] - mx_strlen(pw->pw_name); j++) mx_printchar(' ');
				mx_printchar(' ');
        	
			struct group *g = getgrgid(elem->sub_elems[i]->statbuf.st_gid);
			if (g) {
				mx_printstr(g->gr_name);
				for(int j = 0; j < column_size[2] - mx_strlen(g->gr_name); j++) mx_printchar(' ');
        	}
			else {
				mx_printint(elem->sub_elems[i]->statbuf.st_gid);
				for(int j = 0; j < column_size[2] - mx_int_length(elem->sub_elems[i]->statbuf.st_gid); j++) mx_printchar(' ');
			}
        
			mx_printchar(' ');
			if(flags.h){
				for(int j = 0; j <= column_size[3] - 4; j++) mx_printchar(' ');
				mx_convert_size(elem->sub_elems[i]->statbuf.st_size);
			}
			else{
			for (int j = 0; j <= column_size[3] - mx_int_length(elem->sub_elems[i]->statbuf.st_size); j++)
				mx_printchar(' ');
			mx_printint(elem->sub_elems[i]->statbuf.st_size);
			}
			mx_printchar(' ');
			if(flags.T){
				char *time_str;
				if(flags.u)
					time_str = ctime(&elem->sub_elems[i]->statbuf.st_atime);
				else if(flags.c)
					time_str = ctime(&elem->sub_elems[i]->statbuf.st_ctime);
				else
					time_str = ctime(&elem->sub_elems[i]->statbuf.st_mtime);
				time_str[mx_strlen(time_str) - 1] = '\0';
				time_str += 4;
				mx_printstr(time_str);
			}
			else {
				mx_get_date(elem->sub_elems[i]->statbuf, flags);
			}
		}
		mx_printchar(' ');


		print_filetype_color(elem->sub_elems[i]);
		mx_printstr(elem->sub_elems[i]->name);
		mx_printchar('\n');
		mx_printstr(NO_COLOR);
	}
}
