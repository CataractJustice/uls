#include "uls.h"


void push_elem(ls_fs_elem_t* fs, ls_fs_elem_t* elem) {
	fs->sub_elems[fs->elem_count] = elem;
	fs->elem_count++;
}

int ls_fs_open(ls_fs_elem_t* fs, ls_args_t args) {
	DIR* dir = opendir(args.path);
	if(dir == NULL) {
		return -1;
	}

	fs->full_name = args.path;
	stat(fs->full_name, &(fs->statbuf));
	lstat(fs->full_name, &(fs->statbuf));

	struct dirent* entry;
	int elem_count = 0;
	while((entry = readdir(dir))) {
		elem_count++;
	}
	closedir(dir);

	fs->sub_elems = malloc(sizeof(ls_fs_elem_t*) * elem_count);
	dir = opendir(args.path);
	while((entry = readdir(dir))) {
		ls_fs_elem_t* elem = malloc(sizeof(ls_fs_elem_t));

		elem->elem_count = 0;
		elem->sub_elems = 0;
		elem->name = mx_strdup(entry->d_name);

		if(should_be_displayed(entry->d_name, args.flags)) {
			push_elem(fs, elem);
			ls_args_t sub_args = args;
			char* pathslash = mx_strjoin(sub_args.path, "/");
			sub_args.path = mx_strjoin(pathslash, elem->name);
			bool isdir = mx_isdir(sub_args.path);
			elem->full_name = sub_args.path;
			stat(elem->full_name, &(elem->statbuf));
			lstat(elem->full_name, &(elem->statbuf));

			if(mx_strcmp(elem->name, ".") && mx_strcmp(elem->name, "..") && args.flags.R) {
				free(pathslash);
				if(isdir)
					ls_fs_open(elem, sub_args);
			}
		}

	}
	ls_fs_sort_sub_elems(fs, &ls_sort_name);
	if(args.flags.t) {
		ls_fs_sort_sub_elems(fs, &ls_sort_time);
	}
	return 0;
}
