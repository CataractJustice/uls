#include "uls.h"

int mx_get_console_cols() {
	struct winsize ws;
    ioctl(STDIN_FILENO, TIOCGWINSZ, &ws);
	int col = 80;
	if(isatty(1))
    	col = ws.ws_col; 
	return col;
}

