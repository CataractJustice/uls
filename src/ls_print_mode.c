 #include "uls.h"
 
 void ls_print_mode(const ls_fs_elem_t* elem) {
	if (S_ISLNK(elem->statbuf.st_mode)) mx_printchar('l');
	else if (S_ISCHR(elem->statbuf.st_mode)) mx_printchar('c');
	else if (S_ISBLK(elem->statbuf.st_mode)) mx_printchar('b');
	else if (S_ISFIFO(elem->statbuf.st_mode)) mx_printchar('p');
	else if (S_ISSOCK(elem->statbuf.st_mode)) mx_printchar('s');
	else mx_printchar((S_ISDIR(elem->statbuf.st_mode) ? 'd' : '-'));
	mx_printchar((elem->statbuf.st_mode & S_IRUSR ? 'r' : '-'));
	mx_printchar((elem->statbuf.st_mode & S_IWUSR ? 'w' : '-'));
	if (elem->statbuf.st_mode & S_ISUID) {
		mx_printchar((elem->statbuf.st_mode & S_IXUSR ? 's' : 'S'));
	}
	else mx_printchar((elem->statbuf.st_mode & S_IXUSR ? 'x' : '-'));
	mx_printchar((elem->statbuf.st_mode & S_IRGRP ? 'r' : '-'));
	mx_printchar((elem->statbuf.st_mode & S_IWGRP ? 'w' : '-'));
	if (elem->statbuf.st_mode & S_ISGID) {
		mx_printchar((elem->statbuf.st_mode & S_IXGRP ? 's' : 'S'));
	}
	else mx_printchar((elem->statbuf.st_mode & S_IXUSR ? 'x' : '-'));
	mx_printchar((elem->statbuf.st_mode & S_IROTH ? 'r' : '-'));
	mx_printchar((elem->statbuf.st_mode & S_IWOTH ? 'w' : '-'));
	if (elem->statbuf.st_mode & S_ISVTX) {
		mx_printchar((elem->statbuf.st_mode & S_IXOTH ? 't' : 'T'));
	}
	else mx_printchar((elem->statbuf.st_mode & S_IXUSR ? 'x' : '-'));
 }

