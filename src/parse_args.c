#include "uls.h"

ls_flags_t default_flags() {
	ls_flags_t flags = {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	return flags;
}

void parse_flags(ls_flags_t* flags, const char *flagstr) {
    for (int i = 1; i < mx_strlen(flagstr); i++) {
        switch (flagstr[i]) {
        case 'l':
            flags->l = 1;
            flags->C = 0;
            flags->one = 0;
            flags->without_flags = 0;
            break;
        case 'R':
            flags->R = 1;
            flags->without_flags = 0;
            break;
        case 'a':
            flags->a = 1;
            flags->without_flags = 0;
            break;
        case 'A':
            flags->A = 1;
            flags->without_flags = 0;
            break;
        case 'G':
            flags->G = 1;
            flags->without_flags = 0;
            break;
        case 'h':
            flags->h = 1;
            flags->without_flags = 0;
            break;
        case '@':
            flags->at = 1;
            flags->without_flags = 0;
            break;
        case 'e':
            flags->e = 1;
            flags->without_flags = 0;
            break;
        case 'T':
            flags->T = 1;
            flags->without_flags = 0;
            break;
        case '1':
            flags->one = 1;
            flags->l = 0;
            flags->C = 0;
            flags->without_flags = 0;
            break;
        case 'C':
            flags->C = 1;
            flags->l = 0;
            flags->one = 0;
            flags->without_flags = 0;
            break;
        case 'r':
            flags->r = 1;
            flags->without_flags = 0;
            break;
        case 't':
            flags->t = 1;
            flags->without_flags = 0;
            break;
        case 'u':
            flags->u = 1;
            flags->c = 0;
            flags->without_flags = 0;
            break;
        case 'c':
            flags->c = 1;
            flags->u = 0;
            flags->without_flags = 0;
            break;
        case 'S':
            flags->S = 1;
            flags->without_flags = 0;
            break;
        default:
			mx_printstr("inv flag");
			exit(-1);
            return;
        }
    }
}

ls_args_t parse_args(int argc, char** argv) {
	ls_args_t args = {0, default_flags()};

	int argi = 1;
	
	while(argi < argc && (argv[argi][0] == '-'))
	{
		parse_flags(&args.flags, argv[argi]);
		argi++;
	}

	if(argc > argi) {
		args.path = mx_strdup(argv[argi]);
	}
	else {
		args.path = mx_strdup(".");
	}
	return args;
}

