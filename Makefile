BIN=uls
CLFLAGS=-std=c11 -Wall -Wextra -Werror -Wpedantic -g
all: $(BIN)
$(BIN): inc/*.h src/*.c
	make all -i -C libmx
	mkdir -p obj
	clang $(CLFLAGS) -c src/*.c -I inc -I libmx/inc
	mv *.o obj
	clang $(CLFLAGS) -lacl ./obj/*.o -L libmx -lmx -o $(BIN)

clean:
	rm -rf obj;
	make clean -sC libmx

uninstall: clean
	rm -f libmx/libmx.a
	rm -f $(BIN)

reinstall: uninstall all
