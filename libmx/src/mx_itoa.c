#include "libmx.h"

static int number_length(int number) {
	int length = 0;
	
	if(number == 0)
		return 1;

	if(number < 0)
		length++;

	while (number) {
		number /= 10;
		length++;
	}
	return length;
}

char *mx_itoa(int number) {
	int length = number_length(number);
	char *result = NULL;

	result = mx_strnew(length);
	if (number == 0)
		return mx_strcpy(result, "0");
	if (number == -2147483648)
		return mx_strcpy(result, "-2147483648");
	
	if (number < 0) {
			result[length] = '-';
			number = -number;
	}

	for (int i = 0; i < length; i++) {
		
		result[i] = (number % 10) + '0';
		number /= 10;
	}
	mx_str_reverse(result);
	return result;
}

