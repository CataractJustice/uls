
int mx_int_length(int n) {
	if(n == 0)
		return 1;
	int l = 0;
	while(n) {
		n /= 10;
		l++;
	}
	return l;
}

