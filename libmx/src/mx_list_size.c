#include "libmx.h"

int mx_list_size(t_list *list) {
	int s = 0;
	
	while(list) {
		list = list->next;
		s++;
	}
	
	return s;
}


