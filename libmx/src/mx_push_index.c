#include "libmx.h"

void mx_push_index(t_list **list, void *data, int index) {

	if(!list) return;
	if(!*list) {
		*list = mx_create_node(data);
		return;
	}

	if(index <= 0) {
		mx_push_front(list, data);
		return;
	}
	
	t_list* cursor = *list;
	
	for(int i = 0; i < index - 1; i++) {
		if(cursor->next) {
			cursor = cursor->next;
		}
		else {
			break;
		}
	}
	
	t_list* node = mx_create_node(data);
	node->next = cursor->next;
	cursor->next = node;
}


