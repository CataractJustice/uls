#include "libmx.h"
void mx_printstr_wide (const char* str, char ch, int len) {
	mx_printstr(str);
	for(int i = len - mx_strlen(str); i > 0; i--) {
		mx_printchar(ch);
	}
}

