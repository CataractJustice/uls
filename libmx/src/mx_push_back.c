#include "libmx.h"

void mx_push_back(t_list **list, void *data) {
	if(!list) return;
	if(!*list) {
		*list = mx_create_node(data);
		return;
	}
	
	t_list* end = *list;
	
	while(end->next) {
		end = end->next;
	}
	
	end->next = mx_create_node(data);
}


