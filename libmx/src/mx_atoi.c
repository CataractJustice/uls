#include "libmx.h"

int mx_atoi(const char* str) {
	const char* c = str;
	int sign = 1;
	while(mx_isspace(*c) && *c)
		c++;
	if(*c == '-') {
		sign = -1;
		c++;
	}
	while(mx_isdigit(*c))
		c++;
	c--;
	int mul = 1;
	int n = 0;
	while(mx_isdigit(*c) && c >= str) {
		n += (*c - '0') * mul;
		mul *= 10;
		c--;
	}
	return n * sign;
}

