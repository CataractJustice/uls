#ifndef LS_FLAGS
#define LS_FLAGS
typedef struct ls_flags_s {
	int without_flags;
    int l;
    int R;
    int a;
    int A;
    int G;
    int h;
    int at;
    int e;
    int T;
    int one;
    int C;
    int r;
    int t;
    int u;
    int c;
    int S;
    int files;
}	ls_flags_t;
#endif
