#ifndef LS_ARGS
#define LS_ARGS
#include "ls_flags.h"

typedef struct ls_args_s {
	char* path;
	ls_flags_t flags;
} ls_args_t;
#endif
