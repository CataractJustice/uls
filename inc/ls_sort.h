#ifndef LS_SORT
#define LS_SORT
int ls_sort_name (void* a, void* b);
int ls_sort_time (void* a, void* b);
int ls_sort_size (void* a, void* b);
void ls_fs_sort_sub_elems(ls_fs_elem_t* fs, int(*cmp)(void*, void*));
#endif

