#define _DEFAULT_SOURCE
#include "libmx.h"

#include <dirent.h>
#include <sys/stat.h>
#include <pwd.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <sys/acl.h>
#include <sys/xattr.h>
#include <errno.h>
#include <grp.h>
#include <time.h>

#include "colors.h"
#include "ls_flags.h"
#include "ls_args.h"
#include "fs.h"
#include "ls_sort.h"



int mx_get_console_cols();
void print_ls_elem (ls_flags_t flags, const ls_fs_elem_t* elem);
ls_args_t parse_args(int argc, char** argv);
int ls_fs_open(ls_fs_elem_t* fs, ls_args_t args);
bool should_be_displayed (const char* name, ls_flags_t flags);
void ls_print_mode(const ls_fs_elem_t* elem);
typedef struct stat stat_t;
