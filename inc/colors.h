#define BLUE_COLOR_FOR "\x1B[34m"
#define WHITE_COLOR_FOR "\x1B[37m"
#define GREEN_COLOR_FOR "\x1B[32m"
#define RED_COLOR_FOR "\x1B[31m"
#define MAGENTA_COLOR_FOR "\x1B[35m"
#define CYAN_COLOR_FOR "\x1B[36m"
#define BLACK_COLOR_FOR "\x1B[30m"
#define BROWN_COLOR_FOR "\x1B[33m"

#define GREEN_COLOR_BACK "\x1B[42m"
#define RED_COLOR_BACK "\x1B[41m"
#define CYAN_COLOR_BACK "\x1B[46m"
#define BROWN_COLOR_BACK "\x1B[43m"

#define NO_COLOR "\x1B[0m"

