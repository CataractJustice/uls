typedef struct ls_fs_elem_s {
	struct ls_fs_elem_s** sub_elems;
	int elem_count;
	char* name;
	char* full_name;
	int mtime;
	int size;
	struct stat statbuf;
}	ls_fs_elem_t;

typedef ls_fs_elem_t ls_fs_t;

